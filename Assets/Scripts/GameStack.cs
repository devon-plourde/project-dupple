﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStack : MonoBehaviour {

    private Stack<FallingObject> s;
    public FallingObject stackBase;

    //Use this for initialization
    void Start() {

        s = new Stack<FallingObject>();
        this.AddPiece(stackBase);

    }

    public void AddPiece(FallingObject o)
    {
        if (s.Count > 0)
        {
            FallingObject prevPiece = s.Peek();
            prevPiece.GetComponent<BoxCollider2D>().isTrigger = false;
            o.transform.position = new Vector2(o.transform.position.x,
                                    prevPiece.transform.position.y + 3/*prevPiece.GetComponent<Renderer>().bounds.size.y*/);

            prevPiece.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Kinematic;
            prevPiece.GetComponent<Rigidbody2D>().velocity = new Vector2(0,0);
            //prevPiece.abovePiece = o;
            o.belowPiece = prevPiece;

            prevPiece.gameObject.layer = 8;
            prevPiece.gameObject.tag = "InactiveStackPiece";
        }

        
        o.gameObject.tag = "TopOfStack";
        o.isFalling = false;
        o.stack = this;
        o.GetComponent<BoxCollider2D>().isTrigger = true;
        

        s.Push(o);

        o.rank = s.Count;
        this.transform.parent.GetComponent<PlayerControl>().Piece = o.GetComponent<Rigidbody2D>();
        o.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
    }

    public FallingObject getTop()
    {
        return s.Peek();
    }

    public int getSize()
    {
        return s.Count;
    }
}
