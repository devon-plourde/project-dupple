﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour {

    //Global values for this class:
    public float speed = 5;  //Speed at which the butler sprite moves.

    //References to Unity components:
    [HideInInspector]
    public Rigidbody2D Piece;

    // Use this for initialization
    void Start()
    {
        Piece = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        var movement = Input.GetAxis("Horizontal") * speed;
        Piece.transform.Translate(new Vector2(movement, 0));
    }
}
