﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class SceneTransition : MonoBehaviour {

    public EventSystem eventSystem;
    public Transform prefab;

    //// Use this for initialization
    //void start()
    //{

    //}

    // Update is called once per frame
    void Update()
    {
        //Checks to see if a UI element is selected. If not, it will select the NEW GAME button.
        //This will need to encompas more UI menus, but we only have one right now.
        if (Input.GetAxis("Vertical") != 0 && eventSystem.currentSelectedGameObject == null)
        {
            eventSystem.SetSelectedGameObject(GameObject.Find("NewGameBtn"));
        }
    }

    public void NewGame()
    {
        SceneManager.LoadScene("GameScreen", LoadSceneMode.Single);
    }

    public void Options()
    {
        Instantiate(prefab, new Vector3(0, 0, 0), Quaternion.Inverse(Quaternion.identity));
    }
}
