﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;

public class Butler : MonoBehaviour {

    //Global values for this class:
    public float speed = 5;  //Speed at which the butler sprite moves.
    public int cycleSpeed = 10;  //Speed of the butler's cycling animation.

    //References to Unity components:
    private Rigidbody2D _piece;
    private GameObject _platter;

    //Animation references:
    private Animator _anim;
    private int ButlerSpeedHash = Animator.StringToHash("ButlerSpeed");

    //Useful values to get and set:
    public Vector2 platterPosition { get { return _platter.transform.position; } }

    // Use this for initialization
    void Start () {
        _piece = GetComponent<Rigidbody2D>();
        _platter = this.transform.GetChild(0).gameObject;
        _anim = GetComponent<Animator>();
    }

    //void FixedUpdate()
    //{
    //    ButlerMovement();
    //}

    //private void ButlerMovement()
    //{
    //    //Get the movement of the player and add the force to it's rigid body.
    //    Vector2 movement = new Vector2(Input.GetAxis("Horizontal"), 0);
    //    _piece.AddForce(movement * speed);

    //    //Update the animator with the player's new velocity
    //    _anim.SetFloat(ButlerSpeedHash, _piece.velocity.x / cycleSpeed);
    //}
}
