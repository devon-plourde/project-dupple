﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallingObject : MonoBehaviour {

    //Public variables for the class:
    public float fallingSpeed = 0.05f;
    public float attraction = 2f;
    private int affectedPieces = 10;

    //References to Unity components:
    private Transform trans;

    [HideInInspector]
    public GameStack stack;
    [HideInInspector]
    public FallingObject belowPiece;
    [HideInInspector]
    public float rank;

    //Other useful public attributes:
    public bool isFalling { get; set; }

    // Use this for initialization
    void Start () {
        trans = this.GetComponent<Transform>();
        isFalling = true;
    }
	
	// Update is called once per frame
	public void Update () {

        if (isFalling) {
            fall();
        }
        else if (this.gameObject.CompareTag("TopOfStack") && belowPiece != null){
            belowPiece.moveInStack(this);
        }
	}

    void fall()
    {
        trans.Translate(new Vector2(0, -fallingSpeed));
    }

    void moveInStack(FallingObject abovePiece)
    {
        if (!isFalling && abovePiece != null)
        {
            float xPos = this.transform.position.x;
            float diff = stack.getSize() - affectedPieces;
            if (diff < 0){
                diff = 0;
            }
            float variant = attraction - 1;
            float coefficient = attraction - (variant * (1 - (rank > diff ? rank - diff : 0) / (stack.getSize() - diff)));
            transform.Translate(new Vector2((abovePiece.transform.position.x - xPos) / coefficient, 0));

            if(belowPiece != null)
            {
                belowPiece.moveInStack(this);
            }
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        //Only add the piece if the detector is the top of the stack and the 'other' isn't already added.
        if(this.stack != null && this.gameObject.CompareTag("TopOfStack") && !other.gameObject.CompareTag("InactiveStackPiece"))
        {
            stack.AddPiece(other.GetComponent<FallingObject>());
        }
    }
}
