﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour {

    [Header("Game Parameters")]
    public float spawnRate;
    public int maxObjects;

    private int objectCount = 0;

    [Header("Spawnable Objects")]
    public Transform plate;
    //Other objects to spawn.

	// Use this for initialization
	void Start () {
        StartCoroutine(Spawn());
    }

    public void spawnObject(ObjectTypes o)
    {
        switch (o)
        {
            case ObjectTypes.Plate:
                Instantiate(plate, this.transform.position, Quaternion.Inverse(Quaternion.identity));
                break;
            default:
                Instantiate(plate, this.transform.position, Quaternion.Inverse(Quaternion.identity));
                break;
        }
        objectCount++;
    }

    IEnumerator Spawn()
    {
        while(objectCount < maxObjects)
        {
            spawnObject(ObjectTypes.Plate);
            yield return new WaitForSeconds(spawnRate);
        }
    }
}

public enum ObjectTypes
{
    Plate
}

