﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModalPrefab : MonoBehaviour {

    public Transform prefab;
    // Use this for initialization

    void Start()
    {
        Instantiate(prefab, new Vector3(0, 0, 0), Quaternion.identity);
    }

    // Update is called once per frame
    void Update () {
		
	}
}

