﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platter : FallingObject {

    private Vector2 originalPosition;

	// Use this for initialization
	void Start () {
        originalPosition = transform.localPosition;
	}
	
	// Update is called once per frame
	new void Update () {
        transform.localPosition = originalPosition;
	}
}
